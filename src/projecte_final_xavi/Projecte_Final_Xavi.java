package projecte_final_xavi;
/*Ins Sabadell
CFGS DAM
Xavi Prats*/
import java.util.Scanner;

/**
 * <h2>Aquesta classe simula l'actuaci� d'un professor a l'entrar a classe depenenent de la olor</h2>
 * @author Xavi
 *
 */
public class Projecte_Final_Xavi {
	static Scanner reader = new Scanner(System.in);
	
	/**
	 * En el main introduirem el n�mero de casos de prova, i per cada un introduirem tantes olors com el primer n�mero introduit
	 * @param args �s el main
	 */
	public static void main(String[] args) {
		int cas = reader.nextInt();
		for (int i = 0; i < cas; i++) {
			int pestetotal = 0;
			int pestepudents = 0;
			
			int alumnes = reader.nextInt();
			for (int j = 0; j < alumnes; j++) {
				int olorpersona = reader.nextInt();
				if (olorpersona > 2) { //Si la olor �s major que 3, se sumar� a pestepudents
					pestepudents += olorpersona;
				}
				pestetotal += olorpersona; //Cada olor s'emmagatzema al total
			}
			reader.nextLine();
			String finestra = reader.nextLine();
			System.out.println(joc(pestetotal, pestepudents, finestra)); //Crida joc() i imprimeix el seu return
		}
	}
	
	/**
	 * Aquest m�tode �s cridat en cada cas de prova
	 * @param pestetotal �s la suma de totes les olors
	 * @param pestepudents �s la suma de totes les olors majors que 2
	 * @param finestra �s un string, est� dins d'aquest m�tode per a poder fer la comprovaci� amb JUnit
	 * @return String Retorna TUFO o OK depenent de la difer�ncia entre pestepudents i pestetotal
	 */
	public static String joc(int pestetotal, int pestepudents, String finestra) {
		if (finestra.equalsIgnoreCase("Oberta")) {
			pestepudents *= 0.75; //Si la finestra est� "Oberta" pestepudents disminueix un 25%
		}
		if (pestepudents >= pestetotal*0.5) {
			return("TUFO");
		} else {
			return("OK");
		}
	}
}